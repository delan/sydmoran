\documentclass[a4paper,titlepage,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[margin=1in]{geometry}
\usepackage[british]{babel}
\usepackage{csquotes}
\usepackage[backend=biber,style=apa]{biblatex}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{multirow}
\usepackage[usenames,dvipsnames]{color}
\DeclareLanguageMapping{british}{british-apa}
\addbibresource{report.bib}
\hypersetup{
	pdfauthor=Delan Azabani;
	          Andrew Woods;
	          Joshua Hollick;
	          Petra Helmholz;
	          Paul Bourke,
	pdftitle=Load-balanced rendering of 3D model animations
}
\lstset{basicstyle=\ttfamily, basewidth=0.5em}

\title{Load-balanced rendering of 3D model animations}
\date{February 1, 2016}
\author{Delan Azabani}

\pagenumbering{gobble}
\thispdfpagelabel{0}

\begin{document}

\author{\begin{tabular}{rl}
Author:      & Delan Azabani \texttt{<delan.azabani@curtin.edu.au>} \\ [1em]
Supervisors: & Andrew Woods \texttt{<a.woods@curtin.edu.au>} \\
             & Joshua Hollick \texttt{<joshua.hollick@curtin.edu.au>} \\
             & Petra Helmholz \texttt{<petra.helmholz@curtin.edu.au>} \\
             & Paul Bourke \texttt{<p.bourke@unsw.edu.au>} \\ [1em]
\multicolumn{2}{c}{This project was supported
	by the Pawsey Supercomputing Centre} \\
\multicolumn{2}{c}{through the use of its
	advanced computing resources.}
\end{tabular}}
\maketitle
\pagenumbering{roman}

\vspace*{0.2\vsize}
\hspace{0.1\hsize}
\begin{minipage}{0.8\hsize}

\section*{Abstract}

\end{minipage}

\newpage
\vspace*{0.2\vsize}
\hspace{0.1\hsize}
\begin{minipage}{0.8\hsize}

\section*{Acknowledgements}

In addition to my supervisors, I would like to thank the following people for
their support and guidance. I should add that this list is by no means
exhaustive, nor is it ordered in any meaningful way:

\vspace{0.5\baselineskip}

\begin{itemize}
	\item Brian Skjerven, Pawsey Supercomputing Centre
	\item David Cooper, Curtin University
	\item Valerie Maxville, Pawsey Supercomputing Centre
	\item Cyra Locsin
	\item Ibrahim Azabani
	\item Mei Na Zhi
\end{itemize}

\end{minipage}

\newpage
\tableofcontents
\newpage
\pagenumbering{arabic}

\section{Introduction}

One afternoon in 1941, HMAS \textit{Sydney} encountered a vessel that
purported to be a Dutch cargo ship. After over an hour of peculiar behaviour,
the crew of the \textit{Sydney} discerned that this was a disguise, and
Germany's HSK \textit{Kormoran} revealed herself, engaging the \textit{Sydney}
in a battle that lasted for nearly an hour. By the next morning, both ships
had sunk, with none of those on board the \textit{Sydney} surviving
(\cite{Gill1957}).

The \textit{Sydney} and the \textit{Kormoran} remained sunken for over six
decades before their wrecks were discovered within a week of one another
(\cite{ABC2008b}). More than two kilometres deep and 200 km away from Steep
Point in Western Australia, their historic significance resulted in their
immediate protection under the \textit{Historic Shipwrecks Act 1976}
(\cite{ABC2008a}), and less than three years later, the wrecks were placed on
our Commonwealth Heritage List (\cite{SMH2011}). As a consequence of this, any
expeditions are heavily restricted, and the ability to recover any artefacts
from the sites is virtually impossible.

Not to be dissuaded by these necessary constraints, from May 2015, a team of
researchers and experts from several institutions have started to capture
imagery of the wrecks, leveraging the computing resources provided by Pawsey
and photogrammetry software to create computer models and textures of these
wrecks (\cite{Pawsey2015}). While the results of these efforts are ongoing as
of November, what they have yielded has already provided novel insight into
the circumstances of the battle, and these data could be used pedagogically
when combined with visualisation techniques.

Prior to this project, most of the visualisations that have been created for
these sites have used the Unity video game engine, which provides much of the
software necessary to render them while also providing features like
interactivity. While one of the significant benefits of an engine like Unity
is its ability to create real-time, interactive visualisations, the
constraints of this approach limit the fidelity of any visualisation to what
can be rendered in real-time on one computer, even if the visualisation need
only be rendered ahead of time. This has become an impediment for applications
like documentaries.

Routine optimisations have not proved sufficient to facilitate visualisations
of a high fidelity, where realistic underwater lighting and dynamics are
required, and features like stereoscopic and fish-eye rendering are desirable.
To achieve these, a \textit{ray tracer} such as POV-Ray would be more
appropriate. POV-Ray can yield an inherently more faithful reproduction of the
sites, can render scenes ahead of time, and scales well to supercomputers like
Pawsey's \textit{Magnus} when combined with tools such as SLURM.

\newpage

What remains lacking is adequate tooling to manage this process. For any given
scenario such as the \textit{Sydney-Kormoran} site, assuming that
photogrammetry is complete, a person who wishes to animate a sequence for a
documentary would need to know how to:

\begin{itemize}
	\item convert the model and texture data from the formats that
	      software like PhotoScan, VisualSFM, and Blender use to POV-Ray's
	      formats;
	\item write a scene file for POV-Ray, using its own scene description
	      language;
	\item write a script for SLURM, which appropriately distributes the
	      rendering task across the topology and resources provided by a
	      supercomputer;
	\item write a program to combine the generated frames into a video
	      format that is suitable for further processing; and
	\item mitigate, handle, and otherwise manage the failure modes that
	      are associated with any large job on a supercomputer.
\end{itemize}

Each of these steps requires some amount of intimate knowledge that would
present a barrier to entry to anyone who wishes to create such an animation.
The goal of this project is to improve this workflow, allowing content
creators with minimal programming knowledge to arrange wrecks and other sites
along with a variety of camera sequences, and then animate them on a
supercomputer such as \textit{Magnus}, in a way that allows for future
extension to advanced techniques such as stereoscopic, cylinder, and fish-eye
rendering.

\newpage
\begin{sloppypar}
	\printbibliography
\end{sloppypar}

\end{document}
