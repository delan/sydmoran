#include "consts.inc"
#include "colors.inc"
#include "Decimated.inc"

camera {
	up z
	right y * 16 / 9
	sky z
	// start at the origin
	location o
	// look forwards
	look_at y
	// look upwards and slightly backwards
	rotate 110 * x
	// move forwards
	translate 3.5 * y
}

light_source {
	o
	color White
	spotlight
	point_at z
}

HACS_MESH

background { color DarkSlateGrey }
