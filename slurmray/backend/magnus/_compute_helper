#!/bin/bash -l
set -eu

if [ "_$#" != '_5' ]; then
	>&2 echo "\
_compute_helper - CLE helper for submit_povray_project

SYNOPSIS
	_compute_helper NODES CHUNKS ROOT PROJECT BACKEND

NOTES
	This program should be run on a compute node."
	exit 1
fi

nodes="$1"
chunks="$2"
root="$3"
project="$4"
backend="$5"

cd "$backend"
. ./_configuration

# FIXME: hard-coded value
module load /ivec/cle52/magnus/modulefiles/apps/povray/3.6.1

render_farm="$root/output/$project/render_farm"
chunk_seed="$(basename $(mktemp /tmp/XXXXXXXXXXXXX))"

# Use the 32-bit FNV-1a hash modulo chunks, then use linear probing

chunk_number="$("$root"/util/hash -m "$chunks" -s "$chunk_seed")"

while ! mkdir "$render_farm/$chunk_number/_claimed_chunk" 2> /dev/null > /dev/null; do
	chunk_number=$(((chunk_number + 1) % chunks))
done

>&2 echo "_compute_helper: $chunk_seed: chunk $chunk_number claimed"

povray \
	"$render_farm/$chunk_number/parameters.ini" \
	> "$render_farm/$chunk_number/render.out.log" \
	2> "$render_farm/$chunk_number/render.err.log"

>&2 echo "_compute_helper: $chunk_seed: chunk $chunk_number finished"
