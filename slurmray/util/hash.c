#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define HASH_BASIS 2166136261
#define HASH_PRIME 16777619

int main(int argc, char *argv[]) {
	int retval = 0;
	bool still_want_option_arguments = true;
	bool help_flag = false;
	bool help_needed = false;
	bool have_modulo = false;
	bool have_string = false;
	bool have_file = false;
	uint32_t modulo = 0;
	char *input_string = NULL;
	char *input_file_name = NULL;
	for (int i = 1; i < argc; i++) {
		if (still_want_option_arguments && (
			!strcmp(argv[i], "-h") ||
			!strcmp(argv[i], "-help")
		)) {
			help_flag = true;
		} else if (still_want_option_arguments && (
			!strcmp(argv[i], "-m") ||
			!strcmp(argv[i], "-modulo")
		)) {
			if (++i < argc) {
				modulo = (uint32_t) strtoul(argv[i], NULL, 10);
				have_modulo = true;
				if (modulo == 0) {
					fprintf(
						stderr, "\
%s: the modulo value given at %s must not be zero\n",
						argv[0],
						argv[i - 1]
					);
					help_flag = true;
					help_needed = true;
				}
			} else {
				fprintf(
					stderr, "\
%s: this option requires an argument: %s\n",
					argv[0],
					argv[i - 1]
				);
				help_flag = true;
				help_needed = true;
			}
		} else if (still_want_option_arguments && (
			!strcmp(argv[i], "-s") ||
			!strcmp(argv[i], "-string")
		)) {
			if (++i < argc) {
				input_string = argv[i];
				have_string = true;
			} else {
				fprintf(
					stderr, "\
%s: this option requires an argument: %s\n",
					argv[0],
					argv[i - 1]
				);
				help_flag = true;
				help_needed = true;
			}
		} else if (still_want_option_arguments && (
			!strcmp(argv[i], "--")
		)) {
			still_want_option_arguments = false;
		} else if (still_want_option_arguments && (
			argv[i][0] == '-' &&
			argv[i][1] != '\0'
		)) {
			fprintf(
				stderr, "\
%s: this option is unknown: %s\n",
				argv[0],
				argv[i]
			);
			help_flag = true;
			help_needed = true;
		} else if (have_file) {
			fprintf(
				stderr, "\
%s: this argument is extraneous: %s\n",
				argv[0],
				argv[i]
			);
			help_flag = true;
			help_needed = true;
			still_want_option_arguments = false;
		} else {
			if (strcmp(argv[i], "-")) {
				input_file_name = argv[i];
			}
			have_file = true;
			still_want_option_arguments = false;
		}
	}
	if (have_string && have_file) {
		fprintf(
			stderr, "\
%s: -string may only be used when a file name is not supplied\n",
			argv[0]
		);
		help_flag = true;
		help_needed = true;
	}
	if (help_flag) {
		FILE *output_file = stdout;
		if (help_needed) {
			retval = 0x40;
			output_file = stderr;
		}
		fputs("\
hash - calculate the 32-bit FNV-1a hash of an input\n\
\n\
SYNOPSIS\n\
	hash [-h] [-m divisor] [-s string] [--] [file]\n\
\n\
OPTIONS\n\
	-h, -help       show this help message, and do nothing else\n\
	-m, -modulo     take a modulo of the resultant hash\n\
	-s, -string     provide an input string as an argument\n\
", output_file);
	} else {
		uint32_t hash = HASH_BASIS;
		if (have_string) {
			size_t length = strlen(input_string);
			for (size_t i = 0; i < length; i++) {
				hash ^= (unsigned char) input_string[i];
				hash *= HASH_PRIME;
			}
		} else {
			FILE *input_file = stdin;
			if (input_file_name) {
				input_file = fopen(input_file_name, "rb");
				if (!input_file) {
					perror(argv[0]);
					return 1;
				}
			}
			int value;
			while ((value = getc(input_file)) != EOF) {
				hash ^= (unsigned char) value;
				hash *= HASH_PRIME;
			}
			if (ferror(input_file)) {
				perror(argv[0]);
				retval = 1;
			}
			if (input_file != stdin) {
				if (fclose(input_file) != 0) {
					perror(argv[0]);
					retval = 1;
				}
			}
		}
		if (have_modulo) {
			printf("%" PRIu32 "\n", hash % modulo);
		} else {
			printf("%" PRIu32 "\n", hash);
		}
	}
	return retval;
}
