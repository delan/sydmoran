#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#define OP_MAX_LINE 256
#define OP_MAX_SIZE 1048576
#define POSITION_FORMAT PRId32

typedef int32_t position;

static int retval = 0;
static time_t start_time;

static bool verbose_flag = false;
static char *mesh_union_name = "IMPORTED_MESH";

static char *input_path = NULL;
static FILE *input_file = NULL;
static char input_line[OP_MAX_LINE] = "";
static size_t input_position = 0;

static char *object_path = NULL;
static FILE *object_file = NULL;

static char *material_path = NULL;
static FILE *material_file = NULL;

static char *output_path = NULL;
static FILE *output_file = NULL;

static struct vector {
	float x, y, z;
}

geometric_vertices[OP_MAX_SIZE],
vertex_normals[OP_MAX_SIZE],
texture_vertices[OP_MAX_SIZE];

struct colour {
	enum {
		RGB_IMMEDIATES,
		CIE_XYZ_IMMEDIATES,
		SPECTRAL_CURVE_FILE
	} kind;
	union {
		struct vector RGB;
		struct vector CIE_XYZ;
		struct {
			char *path;
			float factor;
		} spectral_curve;
	} value;
};

struct texture_map {
	struct texture_map *next;
	char *file_path;                                              // map_*
	bool blend_horizontally;                                    // -blendu
	bool blend_vertically;                                      // -blendv
	float bump_multiplier;                                          // -bm
	float sharpness_boost;                                       // -boost
	bool colour_correction;                                          // cc
	bool texture_clamping;                                       // -clamp
	enum {
		RED_CHANNEL,                                              // r
		GREEN_CHANNEL,                                            // g
		BLUE_CHANNEL,                                             // b
		MATTE_CHANNEL,                                            // m
		LUMINANCE_CHANNEL,                                        // l
		DEPTH_CHANNEL                                             // z
	} texture_channel;                                         // -imfchan
	float base_modifier;                                       // -mm base
	float gain_modifier;                                  // -mm base gain
	struct vector translate;                                   // -o u v w
	struct vector scale;                                       // -s u v w
	struct vector turbulence;                                  // -t u v w
	size_t resolution;                                          // -texres
};

static struct material {
	char name[OP_MAX_LINE];
	struct {
		struct colour colour;                                    // Ka
		struct texture_map *colour_maps;                     // map_Ka
	} ambient;
	struct {
		struct colour colour;                                    // Kd
		struct texture_map *colour_maps;                     // map_Kd
	} diffuse;
	struct {
		struct colour colour;                                    // Ks
		struct texture_map *colour_maps;                     // map_Ks
		float specular_exponent;                                 // Ns
		struct texture_map *specular_exponent_maps;          // map_Ns
	} specular;
	struct vector transmission_filter;                               // Tf
	/* illumination_model: mathematical function used for lighting
	 *
	 * The summary provided by the specification doesn't seem very clear
	 * for those who are new to the field, but this may be more helpful:
	 *
 *                              |     Reflection      |    Transparency    |
 * illum  | Model  | Lamb | Bli | Whit | Ray   | Fres | Gla | Refra | Fres | Shadow |
 * number | letter | ert  | nn  | ted  | trace | nel  | ss  | ction | nel  | matte  |
 * ----------------------------------------------------------------------------------
 *    0   |   A    |      |     |      |       |      |     |       |      |        |
 *    1   |   B    |  Y   |     |      |       |      |     |       |      |        |
 *    2   |   C    |  Y   |  Y  |      |       |      |     |       |      |        |
 *    3   |   D    |  Y   |  Y  |  Y   |   Y   |      |     |       |      |        |
 *    4   |   E    |  Y   |  Y  |  Y   |   Y   |      |  Y  |       |      |        |
 *    5   |   F    |  Y   |  Y  |  Y   |   Y   |  Y   |     |       |      |        |
 *    6   |   G    |  Y   |  Y  |      |       |      |     |   Y   |      |        |
 *    7   |   H    |  Y   |  Y  |      |       |  Y   |     |   Y   |  Y   |        |
 *    8   |   I    |  Y   |  Y  |  Y   |       |      |     |       |      |        |
 *    9   |   J    |  Y   |  Y  |  Y   |       |      |  Y  |       |      |        |
 *   10   |   K    |      |     |      |       |      |     |       |      |   Y    |
 * ----------------------------------------------------------------------------------
 	 */
	enum {
		A___CONSTANT_DIFFUSE,
		B___A_PLUS_LAMBERTIAN_DIFFUSE,
		C___B_PLUS_BLINN_SPECULAR,
		D___C_PLUS_WHITTED_ILLUMINATION_WITH_RAY_TRACING,
		E___D_PLUS_GLASS_TRANSPARENCY,
		F___D_PLUS_FRESNEL_REFLECTION,
		G___D_PLUS_REFRACTION_TRANSPARENCY,
		H___D_PLUS_FRESNEL_REFLECTION_AND_REFRACTION_TRANSPARENCY,
		I___C_PLUS_WHITTED_ILLUMINATION_WITHOUT_RAY_TRACING,
		J___I_PLUS_GLASS_TRANSPARENCY,
		K___SHADOW_MATTE_WITH_INVISIBLE_SURFACES
	} illumination_model;                                         // illum
	struct {
		float factor;                                             // d
		struct texture_map *factor_maps;                      // map_d
		bool halo;                                          // d -halo
	} dissolve;
	float optical_density;                                           // Ni
	bool antialias_textures;                                    // map_aat
	struct texture_map *decal_maps;                               // decal
	struct texture_map *displacement_maps;                         // disp
	struct texture_map *bump_maps;                                 // bump
	struct {
		enum {
			SPHERE_REFLECTION,
			CUBE_TOP_REFLECTION,
			CUBE_BOTTOM_REFLECTION,
			CUBE_FRONT_REFLECTION,
			CUBE_BACK_REFLECTION,
			CUBE_LEFT_REFLECTION,
			CUBE_RIGHT_REFLECTION
		} type;                                               // -type
		struct texture_map texture;
		float sharpness;                                  // sharpness
	} reflection_map;                                              // refl
}

materials[OP_MAX_SIZE];

static struct face {
	struct reference {
		position geometric_vertex;
		position texture_vertex;
		position vertex_normal;
	} vertices[3];
}

faces[OP_MAX_SIZE];

static position geometric_vertex_count;
static position vertex_normal_count;
static position texture_vertex_count;
static position material_count;
static position face_count;

static char current_material_name[OP_MAX_LINE] = "";
static bool common_information_printed = false;

bool set_retval(int retval);
bool read_line(void);
bool check_line(void);
bool parse_material_line(void);
bool parse_object_line(void);
bool parse_newmtl(void);
bool parse_map_Kd(void);
bool parse_v(void);
bool parse_vn(void);
bool parse_vt(void);
bool parse_usemtl(void);
bool parse_f(void);
bool convert_mesh(void);

int main(int argc, char *argv[]) {
	start_time = time(NULL);
	bool still_want_option_arguments = true;
	bool help_flag = false;
	bool help_needed = false;
	bool have_object_file = false;
	bool have_material_file = false;
	bool have_output_file = false;
	object_file = stdin;
	material_file = stdin;
	output_file = stdout;
	for (int i = 1; i < argc; i++) {
		if (still_want_option_arguments && (
			!strcmp(argv[i], "-h") ||
			!strcmp(argv[i], "-help")
		)) {
			help_flag = true;
		} else if (still_want_option_arguments && (
			!strcmp(argv[i], "-v") ||
			!strcmp(argv[i], "-verbose")
		)) {
			verbose_flag = true;
		} else if (still_want_option_arguments && (
			!strcmp(argv[i], "-n") ||
			!strcmp(argv[i], "-name")
		)) {
			if (++i < argc) {
				mesh_union_name = argv[i];
			} else {
				fprintf(
					stderr, "\
%s: this option requires an argument: %s\n",
					argv[0],
					argv[i - 1]
				);
				help_flag = true;
				help_needed = true;
			}
		} else if (still_want_option_arguments && (
			!strcmp(argv[i], "--")
		)) {
			still_want_option_arguments = false;
		} else if (still_want_option_arguments && (
			argv[i][0] == '-' &&
			argv[i][1] != '\0'
		)) {
			fprintf(
				stderr, "\
%s: this option is unknown: %s\n",
				argv[0],
				argv[i]
			);
			help_flag = true;
			help_needed = true;
		} else if (
			have_object_file &&
			have_material_file &&
			have_output_file
		) {
			fprintf(
				stderr, "\
%s: this argument is extraneous: %s\n",
				argv[0],
				argv[i]
			);
			help_flag = true;
			help_needed = true;
			still_want_option_arguments = false;
		} else if (
			have_object_file &&
			have_material_file
		) {
			if (strcmp(argv[i], "-")) {
				output_path = argv[i];
			}
			have_output_file = true;
			still_want_option_arguments = false;
		} else if (
			have_object_file
		) {
			if (strcmp(argv[i], "-")) {
				material_path = argv[i];
			}
			have_material_file = true;
			still_want_option_arguments = false;
		} else {
			if (strcmp(argv[i], "-")) {
				object_path = argv[i];
			}
			have_object_file = true;
			still_want_option_arguments = false;
		}
	}
	if (!object_path || !material_path || !output_path) {
		fprintf(
			stderr, "\
%s: an object file, a material file, and an output file must be given\n",
			argv[0]
		);
		help_flag = true;
		help_needed = true;
	}
	if (help_flag) {
		FILE *help_file = stdout;
		if (help_needed) {
			set_retval(0x40);
			help_file = stderr;
		}
		fputs("\
povo - convert Wavefront OBJ and MTL files to POV-Ray source code\n\
\n\
SYNOPSIS\n\
	povo [-h] [-v] [-n name] [--] OBJECT MATERIAL OUTPUT\n\
\n\
DESCRIPTION\n\
	Given an object file, a material file, and an output file name,\n\
	povo will try to convert the first two into equivalent POV-Ray\n\
	source code containing a CSG union of mesh2 objects, where each\n\
	mesh2 uses a given material definition, or none at all.\n\
\n\
OPTIONS\n\
	-h, -help       show this help message, and do nothing else\n\
	-v, -verbose    show the progress of the parsing and conversion\n\
	-n, -name       override the name of the generated macro\n\
	                default: IMPORTED_MESH\n\
\n\
RETURN VALUES\n\
	Reasons with higher return values take precedence, except for 1,\n\
	which takes precedence over all other return values.\n\
\n\
	0               the conversion completed successfully\n\
	1               there was an otherwise uncategorised error\n\
	64              one or more of the given arguments were invalid\n\
	65              there was a file input and/or output error\n\
	66              there was a syntax error in the input\n\
\n\
", help_file);
		if (!verbose_flag || (help_needed && !help_flag)) {
			fputs("\
(for a more comprehensive guide to using povo, try povo -h -v | less)\n\
", help_file);
		} else {
			fputs("\
IMPLEMENTATION STATUS\n\
	The conversion has been succesfully tested with OBJ/MTL files from:\n\
\n\
	  * Agisoft PhotoScan 1.2.1\n\
	  * Agisoft PhotoScan 1.2.2\n\
\n\
	The following features are supported:\n\
\n\
	  * The object command v (except for vertices with four components)\n\
	  * The object command vn (with an automatic fix for zero vectors)\n\
	  * The object command vt (only for vertices with two components)\n\
	  * The object command usemtl (complete)\n\
	  * The object command f (except for faces with more than four\n\
	    vertex groups, and faces with any missing vt/vn references)\n\
	  * The material command newmtl (complete)\n\
	  * The material command map_Kd (no options are supported yet, and\n\
	    formats other than image files are not yet handled correctly)\n\
\n\
	The following features are not yet supported:\n\
\n\
	  * Textures other than of the two-dimensional variety\n\
	  * Comments in object or material files\n\
	  * The object command vp\n\
	  * The object commands cstype, deg, bmat, and step\n\
	  * The object commands p, l, curv, curv2, and surf\n\
	  * The object commands parm, trim, hole, scrv, sp, and end\n\
	  * The object command con\n\
	  * The object commands g, s, mg, and o\n\
	  * The object commands bevel, c_interp, d_interp, lod, maplib,\n\
	    usemap, mtllib, shadow_obj, trace_obj, ctech, and stech\n\
	  * The object commands bsp, bzp, cdc, cdp, and res\n\
	  * The material commands Ka, Kd, Ks, Tf, illum, d, Ns, sharpness\n\
	    and Ni\n\
	  * The material commands map_Ka, map_Ks, map_Ns, map_d, map_aat,\n\
	    decal, disp, and bump\n\
	  * The texture map options -blendu, -blendv, -bm, -boost, -cc,\n\
	    -clamp, -imfchan, -mm, -o, -s, -t, and -texres\n\
	  * The material command refl\n\
\n\
	Current limitations of this implementation include:\n\
\n\
	  * The parsing is line oriented, so there must not be more than\n\
	    one command on each line, even though such files are valid\n\
\n\
	  * Lines in input files must not exceed 254 characters, which is a\n\
	    limitation designed to avoid using the heap where possible\n\
\n\
	  * A material file must be externally requested, even if it would\n\
	    be empty, and no more than one material file may be used\n\
\n\
	  * Object files must have texture vertices and vertex normals\n\
", help_file);
		}
		goto fail;
	}
	if (object_path) {
		object_file = fopen(object_path, "r");
		if (object_file) {
			fprintf(
				stderr, "\
%s: opened the object file for reading\n",
				object_path
			);
		} else {
			perror(object_path);
			set_retval(0x41);
			goto fail;
		}
	} else {
		object_path = "<stdin>";
	}
	if (material_path) {
		material_file = fopen(material_path, "r");
		if (material_file) {
			fprintf(
				stderr, "\
%s: opened the material file for reading\n",
				material_path
			);
		} else {
			perror(material_path);
			set_retval(0x41);
			goto fail;
		}
	} else {
		material_path = "<stdin>";
	}
	if (output_path) {
		output_file = fopen(output_path, "wb");
		if (output_file) {
			fprintf(
				stderr, "\
%s: opened the POV-Ray file for reading\n",
				output_path
			);
		} else {
			perror(output_path);
			set_retval(0x41);
			goto fail;
		}
	} else {
		output_path = "<stdout>";
	}
	input_path = material_path;
	input_file = material_file;
	if (material_file != object_file) {
		input_position = 0;
	}
	while (read_line() && check_line() && parse_material_line()) {
		/* The body of this loop is intentionally empty */
	}
	if (retval != 0) {
		goto fail;
	}
	input_path = object_path;
	input_file = object_file;
	if (material_file != object_file) {
		input_position = 0;
	}
	while (read_line() && check_line() && parse_object_line()) {
		/* The body of this loop is intentionally empty */
	}
	if (retval != 0) {
		goto fail;
	}
	if (!convert_mesh()) {
		goto fail;
	}
	if (retval == 0) {
		if (fprintf(
			output_file,
			"}\n"
			"\n"
			"#undef IMPORTED_MESH_COMMON_INFORMATION\n"
		) < 0) {
			perror(output_path);
			set_retval(0x41);
			goto fail;
		}
	}
fail:
	if (fflush(output_file) != 0) {
		perror(output_path);
		set_retval(0x41);
	}
	if (fflush(material_file) != 0) {
		perror(material_path);
		set_retval(0x41);
	}
	if (fflush(object_file) != 0) {
		perror(object_path);
		set_retval(0x41);
	}
	if (output_file != stdout && fclose(output_file) != 0) {
		perror(output_path);
		set_retval(0x41);
	}
	if (material_file != stdin && fclose(material_file) != 0) {
		perror(material_path);
		set_retval(0x41);
	}
	if (object_file != stdin && fclose(object_file) != 0) {
		perror(object_path);
		set_retval(0x41);
	}
	if (retval == 0) {
		fprintf(
			stderr, "\
%s: successfully completed in %0.1lf seconds\n",
			argv[0],
			difftime(time(NULL), start_time)
		);
	}
	return retval;
}

bool set_retval(int new_retval) {
	bool good = true;
	switch (new_retval) {
		case 0:
		case 1:
		case 0x40:
		case 0x41:
		case 0x42:
			if (new_retval == 1 || new_retval > retval) {
				retval = new_retval;
			}
			break;
		default:
			fprintf(
				stderr, "\
povo: internal error: set_retval() was called with an invalid value: %d\n",
				new_retval
			);
			retval = 1;
			good = false;
			break;
	}
	return good;
}

bool read_line(void) {
	char *result = fgets(input_line, sizeof(input_line), input_file);
	if (result != NULL) {
		return true;
	} else if (!ferror(input_file)) {
		return false;
	} else {
		perror(input_path);
		set_retval(0x41);
		return false;
	}
}

bool check_line(void) {
	char *cursor = input_line;
	while (*cursor != '\n' && *cursor != '\0') {
		cursor++;
	}
	input_position++;
	if (*cursor == '\n') {
		*cursor = '\0';
		return true;
	} else {
		fprintf(
			stderr, "\
%s:%zu: this line is either too long or missing a terminator\n",
			input_path,
			input_position
		);
		set_retval(0x42);
		return false;
	}
}

bool parse_material_line(void) {
	char *token, *start = input_line;
	if ((token = strtok(start, " "))) {
		if (!strcmp(token, "newmtl")) {
			return parse_newmtl();
		} else if (!strcmp(token, "map_Kd")) {
			return parse_map_Kd();
		} else if (!strcmp(token, "v")) {
			/* Skip this line */
			return true;
		} else if (!strcmp(token, "vt")) {
			/* Skip this line */
			return true;
		} else if (!strcmp(token, "vn")) {
			/* Skip this line */
			return true;
		} else if (!strcmp(token, "usemtl")) {
			/* Skip this line */
			return true;
		} else if (!strcmp(token, "f")) {
			/* Skip this line */
			return true;
		} else {
			if (verbose_flag) {
				fprintf(
					stderr, "\
%s:%zu: warning: this command is unknown: %s\n",
					input_path,
					input_position,
					token
				);
			}
			return true;
		}
	} else {
		/* The input line seems to be empty */
		return true;
	}
}

bool parse_object_line(void) {
	char *token, *start = input_line;
	if ((token = strtok(start, " "))) {
		if (!strcmp(token, "newmtl")) {
			/* Skip this line */
			return true;
		} else if (!strcmp(token, "map_Kd")) {
			/* Skip this line */
			return true;
		} else if (!strcmp(token, "v")) {
			return parse_v();
		} else if (!strcmp(token, "vt")) {
			return parse_vt();
		} else if (!strcmp(token, "vn")) {
			return parse_vn();
		} else if (!strcmp(token, "usemtl")) {
			return parse_usemtl();
		} else if (!strcmp(token, "f")) {
			return parse_f();
		} else {
			if (verbose_flag) {
				fprintf(
					stderr, "\
%s:%zu: warning: this command is unknown: %s\n",
					input_path,
					input_position,
					token
				);
			}
			return true;
		}
	} else {
		/* The input line seems to be empty */
		return true;
	}
}

bool parse_newmtl(void) {
	bool good = true;
	char *token = NULL;
	size_t token_count = 0;
	while ((token = strtok(NULL, " ")) && ++token_count && good) {
		switch (token_count) {
		case 1:
			material_count++;
			strncpy(
				materials[material_count - 1].name,
				token,
				OP_MAX_LINE - 1
			);
			materials[material_count - 1].
				name[OP_MAX_LINE - 1] = '\0';
			break;
		default:
			fprintf(
				stderr, "\
%s:%zu: encountered an extraneous token: %s\n",
				input_path,
				input_position,
				token
			);
			set_retval(0x42);
			good = false;
			goto fail;
			break;
		}
	}
	if (token_count < 1) {
		fprintf(
			stderr, "\
%s:%zu: expected a material name but none was encountered\n",
			input_path,
			input_position
		);
		set_retval(0x42);
		good = false;
		goto fail;
	}
fail:
	return good;
}

/* TODO: refactor the texture initialisation and destruction code */

bool parse_map_Kd(void) {
	bool good = true;
	struct texture_map *colour_map = malloc(sizeof *colour_map);
	if (!colour_map) {
		fprintf(
			stderr, "\
%s:%zu: internal error: malloc(3) returned NULL\n",
			input_path,
			input_position
		);
		set_retval(0x1);
		good = false;
		goto fail;
	}
	char *token = NULL;
	size_t token_count = 0;
	bool still_want_option_arguments = true;
	bool still_want_other_arguments = true;
	while ((token = strtok(NULL, " ")) && ++token_count && good) {
		if (token[0] == '-' && still_want_other_arguments) {
			if (
				still_want_option_arguments &&
				!strcmp(token, "-blendu")
			) {
				fprintf(
					stderr, "\
%s:%zu: TODO: warning: the -blendu option to map_Kd is not yet supported\n",
					input_path,
					input_position
				);
				set_retval(0x42);
				good = false;
				goto fail;
			} else if (
				still_want_option_arguments &&
				!strcmp(token, "-blendv")
			) {
				fprintf(
					stderr, "\
%s:%zu: TODO: warning: the -blendv option to map_Kd is not yet supported\n",
					input_path,
					input_position
				);
				set_retval(0x42);
				good = false;
				goto fail;
			} else if (
				still_want_option_arguments &&
				!strcmp(token, "-boost")
			) {
				fprintf(
					stderr, "\
%s:%zu: TODO: warning: the -boost option to map_Kd is not yet supported\n",
					input_path,
					input_position
				);
				set_retval(0x42);
				good = false;
				goto fail;
			} else if (
				still_want_option_arguments &&
				!strcmp(token, "-cc")
			) {
				fprintf(
					stderr, "\
%s:%zu: TODO: warning: the -cc option to map_Kd is not yet supported\n",
					input_path,
					input_position
				);
				set_retval(0x42);
				good = false;
				goto fail;
			} else if (
				still_want_option_arguments &&
				!strcmp(token, "-clamp")
			) {
				fprintf(
					stderr, "\
%s:%zu: TODO: warning: the -clamp option to map_Kd is not yet supported\n",
					input_path,
					input_position
				);
				set_retval(0x42);
				good = false;
				goto fail;
			} else if (
				still_want_option_arguments &&
				!strcmp(token, "-mm")
			) {
				fprintf(
					stderr, "\
%s:%zu: TODO: warning: the -mm option to map_Kd is not yet supported\n",
					input_path,
					input_position
				);
				set_retval(0x42);
				good = false;
				goto fail;
			} else if (
				still_want_option_arguments &&
				!strcmp(token, "-o")
			) {
				fprintf(
					stderr, "\
%s:%zu: TODO: warning: the -o option to map_Kd is not yet supported\n",
					input_path,
					input_position
				);
				set_retval(0x42);
				good = false;
				goto fail;
			} else if (
				still_want_option_arguments &&
				!strcmp(token, "-s")
			) {
				fprintf(
					stderr, "\
%s:%zu: TODO: warning: the -s option to map_Kd is not yet supported\n",
					input_path,
					input_position
				);
				set_retval(0x42);
				good = false;
				goto fail;
			} else if (
				still_want_option_arguments &&
				!strcmp(token, "-t")
			) {
				fprintf(
					stderr, "\
%s:%zu: TODO: warning: the -t option to map_Kd is not yet supported\n",
					input_path,
					input_position
				);
				set_retval(0x42);
				good = false;
				goto fail;
			} else if (
				still_want_option_arguments &&
				!strcmp(token, "-texres")
			) {
				fprintf(
					stderr, "\
%s:%zu: TODO: warning: the -texres option to map_Kd is not yet supported\n",
					input_path,
					input_position
				);
				set_retval(0x42);
				good = false;
				goto fail;
			} else {
				fprintf(
					stderr, "\
%s:%zu: this option to map_Kd is unknown: %s\n",
					input_path,
					input_position,
					token
				);
				set_retval(0x42);
				good = false;
				goto fail;
			}
		} else if (token[0] == '-') {
			fprintf(
				stderr, "\
%s:%zu: option arguments are not allowed after non-option arguments\n",
				input_path,
				input_position
			);
			set_retval(0x42);
			good = false;
			goto fail;
		} else if (still_want_other_arguments) {
			colour_map->file_path = malloc(OP_MAX_LINE);
			if (!colour_map->file_path) {
				fprintf(
					stderr, "\
%s:%zu: internal error: malloc(3) returned NULL\n",
					input_path,
					input_position
				);
				set_retval(1);
				good = false;
				goto fail;
			}
			strncpy(
				colour_map->file_path,
				token,
				OP_MAX_LINE - 1
			);
			colour_map->file_path[OP_MAX_LINE - 1] = '\0';
			still_want_other_arguments = false;
		} else {
			fprintf(
				stderr, "\
%s:%zu: this argument is extraneous: %s\n",
				input_path,
				input_position,
				token
			);
			set_retval(0x42);
			good = false;
			goto fail;
		}
	}
	if (!colour_map->file_path || !*colour_map->file_path) {
		fprintf(
			stderr, "\
%s:%zu: expected a file path but none was encountered\n",
			input_path,
			input_position
		);
		set_retval(0x42);
		good = false;
		goto fail;
	}
	/* Insert the new texture map at the start of the linked list
	 * of diffuse colour maps. The order of the colour maps is not
	 * significant because all of the values are multiplied. */
	colour_map->next =
		materials[material_count - 1].diffuse.colour_maps;
	materials[material_count - 1].diffuse.colour_maps =
		colour_map;
	goto success;
fail:
	free(colour_map->file_path);
	free(colour_map);
success:
	return good;
}

bool parse_v(void) {
	bool good = true;
	char *token = NULL;
	size_t token_count = 0;
	struct vector result = { 0.0, 0.0, 0.0 };
	while ((token = strtok(NULL, " ")) && ++token_count && good) {
		float value;
		if (sscanf(token, "%f", &value) != 1) {
			fprintf(
				stderr, "\
%s:%zu: expected a real number but instead encountered: %s\n",
				input_path,
				input_position,
				token
			);
			good = false;
		} else switch (token_count) {
		case 1:
			result.x = value;
			break;
		case 2:
			result.y = value;
			break;
		case 3:
			result.z = value;
			break;
/* TODO: handle weights, rational curves, and rational surfaces */
		case 4:
		default:
			fprintf(
				stderr, "\
%s:%zu: encountered an extraneous token: %s\n",
				input_path,
				input_position,
				token
			);
			good = false;
			break;
		}
	}
	if (token_count < 3) {
		fprintf(
			stderr, "\
%s:%zu: expected three real numbers but only encountered %zu\n",
			input_path,
			input_position,
			token_count
		);
		good = false;
	}
	if (good) {
		if (geometric_vertex_count == OP_MAX_SIZE) {
			fprintf(
				stderr, "\
%s:%zu: unable to store geometric vertex; limit of %" POSITION_FORMAT " \
was reached\n",
				input_path,
				input_position,
				OP_MAX_SIZE
			);
			good = false;
		} else {
			geometric_vertices[geometric_vertex_count++] = result;
			if (
				verbose_flag &&
				geometric_vertex_count % 100000 == 0
			) {
				fprintf(
					stderr, "\
%s:%zu: progress: parsed %" POSITION_FORMAT " geometric vertices\n",
					input_path,
					input_position,
					geometric_vertex_count
				);
			}
		}
	}
	return good;
}

bool parse_vn(void) {
	bool good = true;
	char *token = NULL;
	size_t token_count = 0;
	struct vector result = { 0.0, 0.0, 0.0 };
	while ((token = strtok(NULL, " ")) && ++token_count && good) {
		float value;
		if (sscanf(token, "%f", &value) != 1) {
			fprintf(
				stderr, "\
%s:%zu: expected a real number but instead encountered: %s\n",
				input_path,
				input_position,
				token
			);
			good = false;
		} else switch (token_count) {
		case 1:
			result.x = value;
			break;
		case 2:
			result.y = value;
			break;
		case 3:
			result.z = value;
			break;
		default:
			fprintf(
				stderr, "\
%s:%zu: encountered an extraneous token: %s\n",
				input_path,
				input_position,
				token
			);
			good = false;
			break;
		}
	}
	if (token_count < 3) {
		fprintf(
			stderr, "\
%s:%zu: expected three real numbers but only encountered %zu\n",
			input_path,
			input_position,
			token_count
		);
		good = false;
	}
	if (good) {
		if (vertex_normal_count == OP_MAX_SIZE) {
			fprintf(
				stderr, "\
%s:%zu: unable to store vertex normal; limit of %" POSITION_FORMAT " \
was reached\n",
				input_path,
				input_position,
				OP_MAX_SIZE
			);
			good = false;
		} else {
			if (
				result.x == 0.0 &&
				result.y == 0.0 &&
				result.z == 0.0
			) {
				if (verbose_flag) {
					fprintf(
						stderr, "\
%s:%zu: warning: vertex normals of <0, 0, 0> are invalid; using <1, 0, 0>\n",
						input_path,
						input_position
					);
				}
				result.x = 1.0;
			}
			vertex_normals[vertex_normal_count++] = result;
			if (
				verbose_flag &&
				vertex_normal_count % 100000 == 0
			) {
				fprintf(
					stderr, "\
%s:%zu: progress: parsed %" POSITION_FORMAT " vertex normals\n",
					input_path,
					input_position,
					vertex_normal_count
				);
			}
		}
	}
	return good;
}

bool parse_vt(void) {
	bool good = true;
	char *token = NULL;
	size_t token_count = 0;
	struct vector result = { 0.0, 0.0, 0.0 };
	while ((token = strtok(NULL, " ")) && ++token_count && good) {
		float value;
		if (sscanf(token, "%f", &value) != 1) {
			fprintf(
				stderr, "\
%s:%zu: expected a real number but instead encountered: %s\n",
				input_path,
				input_position,
				token
			);
			good = false;
		} else switch (token_count) {
		case 1:
			result.x = value;
			break;
		case 2:
			result.y = value;
			break;
		default:
			fprintf(
				stderr, "\
%s:%zu: encountered an extraneous token: %s\n",
				input_path,
				input_position,
				token
			);
			good = false;
			break;
		}
	}
	/* TODO: handle 1D and 3D textures */
	if (token_count < 2) {
		fprintf(
			stderr, "\
%s:%zu: expected two real numbers but only encountered %zu\n",
			input_path,
			input_position,
			token_count
		);
		good = false;
	}
	if (good) {
		if (texture_vertex_count == OP_MAX_SIZE) {
			fprintf(
				stderr, "\
%s:%zu: unable to store texture vertex; limit of %" POSITION_FORMAT " \
was reached\n",
				input_path,
				input_position,
				OP_MAX_SIZE
			);
			good = false;
		} else {
			texture_vertices[texture_vertex_count++] = result;
			if (
				verbose_flag &&
				texture_vertex_count % 100000 == 0
			) {
				fprintf(
					stderr, "\
%s:%zu: progress: parsed %" POSITION_FORMAT " texture vertices\n",
					input_path,
					input_position,
					texture_vertex_count
				);
			}
		}
	}
	return good;
}

bool parse_usemtl(void) {
	/* Flush any previously parsed faces */
	if (face_count > 0 && !convert_mesh())
		return false;
	bool good = true;
	char *token = NULL;
	size_t token_count = 0;
	while ((token = strtok(NULL, " ")) && ++token_count && good) {
		switch (token_count) {
		case 1:
			strncpy(
				current_material_name,
				token,
				OP_MAX_LINE - 1
			);
			current_material_name[OP_MAX_LINE - 1] = '\0';
			face_count = 0;
			break;
		default:
			fprintf(
				stderr, "\
%s:%zu: encountered an extraneous token: %s\n",
				input_path,
				input_position,
				token
			);
			good = false;
			break;
		}
	}
	if (token_count < 1) {
		fprintf(
			stderr, "\
%s:%zu: expected a material name but none was encountered\n",
			input_path,
			input_position
		);
		good = false;
	}
	return good;
}

bool parse_f(void) {
	bool good = true;
	char *token = NULL, *token_cursor = NULL;
	size_t token_count = 0;
	size_t reference_count = 0;
	position current_reference = 0;
	/* Iterate over the tokens, which should be face vertex groups */
	while ((token = strtok(NULL, " ")) && ++token_count && good) {
		int value;
		sscanf(token, "%d", &value);
		/* Check for input lines with too many face vertex groups */
		switch (token_count) {
		case 1:
		case 2:
		case 3:
			token_cursor = token;
			reference_count = 0;
			current_reference = 0;
			/* Iterate over the characters inside a group */
			while (*token_cursor && good) {
				/* Parse the digits of the references */
				switch (*token_cursor) {
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					if (reference_count == 0) {
						reference_count++;
						current_reference = 0;
					}
					current_reference *= 10;
					current_reference += (position) (
						*token_cursor - '0'
					);
					break;
				case '/':
					/* Flush the previous reference */
					switch (reference_count) {
					case 0:
						fprintf(
							stderr, "\
%s:%zu:%td: geometric vertex numbers must not be omitted\n",
							input_path,
							input_position,
							(
								token_cursor -
								input_line + 1
							)
						);
						good = false;
						break;
					case 1:
						if (
							face_count ==
							OP_MAX_SIZE
						) {
							fprintf(
								stderr, "\
%s:%zu: unable to store texture vertex; limit of %" POSITION_FORMAT " \
was reached\n",
								input_path,
								input_position,
								OP_MAX_SIZE
							);
							return false;
						}
						faces[face_count].
							vertices[
								token_count -
								1
							].
							geometric_vertex =
							current_reference;
						reference_count++;
						current_reference = 0;
						break;
					case 2:
						faces[face_count].
							vertices[
								token_count -
								1
							].
							texture_vertex =
							current_reference;
						reference_count++;
						current_reference = 0;
						break;
					default:
						fprintf(
							stderr, "\
%s:%zu:%td: encountered an extraneous character: %c\n",
							input_path,
							input_position,
							(
								token_cursor -
								input_line + 1
							),
							*token_cursor
						);
						good = false;
						break;
					}
					break;
				default:
					fprintf(
						stderr, "\
%s:%zu:%td: expected a decimal digit or a solidus but encountered: %c\n",
						input_path,
						input_position,
						(
							token_cursor -
							input_line + 1
						),
						*token_cursor
					);
					good = false;
					break;
				}
				token_cursor++;
			}
			/* Flush the remainder of face vertex group */
			switch (reference_count) {
			case 0:
				fprintf(
					stderr, "\
%s:%zu: failed to find at least one reference in each face vertex group\n",
					input_path,
					input_position
				);
				good = false;
				break;
			case 1:
				/* Nothing more to be done here */
				break;
			case 2:
				/* Nothing more to be done here either */
				break;
			case 3:
				faces[face_count].
					vertices[token_count - 1].
					vertex_normal =
					current_reference;
				break;
			default:
				fprintf(
					stderr, "\
%s:%zu: encountered an extraneous reference number: %s\n",
					input_path,
					input_position,
					token
				);
				good = false;
				break;
			}
			break;
		default:
			fprintf(
				stderr, "\
%s:%zu: encountered an extraneous token: %s\n",
				input_path,
				input_position,
				token
			);
			good = false;
			break;
		}
	}
	if (good) {
		face_count++;
		if (verbose_flag && face_count % 100000 == 0) {
			fprintf(
				stderr, "\
%s:%zu: progress: parsed %" POSITION_FORMAT " faces\n",
				input_path,
				input_position,
				face_count
			);
		}
	}
	return good;
}

bool convert_mesh(void) {
	if (geometric_vertex_count == 0) {
		fprintf(
			stderr, "\
%s:%zu: this model has no geometric vertices\n",
			input_path,
			input_position
		);
		return false;
	}
	/* TODO: this check would need to be refined if support for surfaces
	 * other than face elements is to be introduced. */
	if (face_count == 0) {
		fprintf(
			stderr, "\
%s:%zu: this model has no faces\n",
			input_path,
			input_position
		);
		return false;
	}
	bool good = true;
	if (!common_information_printed) {
		fprintf(
			stderr, "\
%s:%zu: successfully parsed %" POSITION_FORMAT " geometric vertices\n",
			input_path,
			input_position,
			geometric_vertex_count
		);
		fprintf(
			stderr, "\
%s:%zu: successfully parsed %" POSITION_FORMAT " vertex normals\n",
			input_path,
			input_position,
			vertex_normal_count
		);
		fprintf(
			stderr, "\
%s:%zu: successfully parsed %" POSITION_FORMAT " texture vertices\n",
			input_path,
			input_position,
			texture_vertex_count
		);
		good &= fprintf(
			output_file,
			"#macro IMPORTED_MESH_COMMON_INFORMATION()\n"
			"	vertex_vectors {\n"
			"		%" POSITION_FORMAT ",\n",
			geometric_vertex_count
		) >= 0;
		for (position i = 0; i < geometric_vertex_count; i++)
			good &= fprintf(
				output_file,
				"		<%f, %f, %f>%s",
				geometric_vertices[i].x,
				geometric_vertices[i].y,
				geometric_vertices[i].z,
				(i == geometric_vertex_count - 1) ?
					"\n" : ",\n"
			) >= 0;
		good &= fprintf(
			output_file,
			"	}\n"
		) >= 0;
		if (vertex_normal_count > 0) {
			good &= fprintf(
				output_file,
				"\n"
				"	normal_vectors {\n"
				"		%" POSITION_FORMAT ",\n",
				vertex_normal_count
			) >= 0;
			for (position i = 0; i < vertex_normal_count; i++)
				good &= fprintf(
					output_file,
					"		<%f, %f, %f>%s",
					vertex_normals[i].x,
					vertex_normals[i].y,
					vertex_normals[i].z,
					(i == vertex_normal_count - 1) ?
						"\n" : ",\n"
				) >= 0;
			good &= fprintf(
				output_file,
				"	}\n"
			) >= 0;
		}
		if (texture_vertex_count > 0) {
			good &= fprintf(
				output_file,
				"\n"
				"	uv_vectors {\n"
				"		%" POSITION_FORMAT ",\n",
				texture_vertex_count
			) >= 0;
			for (position i = 0; i < texture_vertex_count; i++)
				good &= fprintf(
					output_file,
					"		<%f, %f>%s",
					texture_vertices[i].x,
					texture_vertices[i].y,
					(i == texture_vertex_count - 1) ?
						"\n" : ",\n"
				) >= 0;
			good &= fprintf(
				output_file,
				"	}\n"
			) >= 0;
		}
		good &= fprintf(
			output_file,
			"#end\n"
			"\n"
			"#declare %s = union {\n",
			mesh_union_name
		) >= 0;
		common_information_printed = true;
	}
	if (*current_material_name) {
		fprintf(
			stderr, "\
%s:%zu: successfully parsed a mesh with %" POSITION_FORMAT " \
faces (material %s)\n",
			input_path,
			input_position,
			face_count,
			current_material_name
		);
	} else {
		fprintf(
			stderr, "\
%s:%zu: successfully parsed a mesh with %" POSITION_FORMAT " \
faces (no material)\n",
			input_path,
			input_position,
			face_count
		);
	}
	/* TODO: handle faces sans texture vertices and/or vertex normals */
	good &= fprintf(
		output_file,
		"	mesh2 {\n"
		"		IMPORTED_MESH_COMMON_INFORMATION()\n"
		"\n"
		"		face_indices {\n"
		"			%" POSITION_FORMAT ",\n",
		face_count
	) >= 0;
	for (position i = 0; i < face_count; i++) {
		good &= fprintf(
			output_file,
			"			<%" POSITION_FORMAT ", %"
			                           POSITION_FORMAT ", %"
			                           POSITION_FORMAT ">%s",
			faces[i].vertices[0].geometric_vertex - 1,
			faces[i].vertices[1].geometric_vertex - 1,
			faces[i].vertices[2].geometric_vertex - 1,
			(i == face_count - 1) ? "\n" : ",\n"
		) >= 0;
	}
	good &= fprintf(
		output_file,
		"		}\n"
		"\n"
		"		uv_indices {\n"
		"			%" POSITION_FORMAT ",\n",
		face_count
	) >= 0;
	for (position i = 0; i < face_count; i++) {
		good &= fprintf(
			output_file,
			"			<%" POSITION_FORMAT ", %"
			                           POSITION_FORMAT ", %"
			                           POSITION_FORMAT ">%s",
			faces[i].vertices[0].texture_vertex - 1,
			faces[i].vertices[1].texture_vertex - 1,
			faces[i].vertices[2].texture_vertex - 1,
			(i == face_count - 1) ? "\n" : ",\n"
		) >= 0;
	}
	good &= fprintf(
		output_file,
		"		}\n"
		"\n"
		"		normal_indices {\n"
		"			%" POSITION_FORMAT ",\n",
		face_count
	) >= 0;
	for (position i = 0; i < face_count; i++) {
		good &= fprintf(
			output_file,
			"			<%" POSITION_FORMAT ", %"
			                           POSITION_FORMAT ", %"
			                           POSITION_FORMAT ">%s",
			faces[i].vertices[0].vertex_normal - 1,
			faces[i].vertices[1].vertex_normal - 1,
			faces[i].vertices[2].vertex_normal - 1,
			(i == face_count - 1) ? "\n" : ",\n"
		) >= 0;
	}
	good &= fprintf(
		output_file,
		"		}\n"
		"\n"
		/* "Parse Error: uv_mapping must be specified before texture."
		 *
		 * If the uv_mapping keyword is to be specified, it must only
		 * be specified once, and strictly before any pigment, normal,
		 * or texture OBJECT_MODIFIERS. */
		"		uv_mapping pigment {\n"
		"			color rgb <1, 0, 1>\n"
		"		}\n"
	) >= 0;
	/* TODO: use a more efficient data structure than a linked list */
	for (position i = 0; i < material_count; i++) {
		if (!strcmp(materials[i].name, current_material_name)) {
			struct texture_map *diffuse_map =
				materials[i].diffuse.colour_maps;
			while (diffuse_map) {
				good &= fprintf(
					output_file,
"\n"
"		pigment {\n"
"			image_map {\n"
"				\"%s\"\n" /* FIXME: this may need escaping */
"			}\n"
"		}\n",
					diffuse_map->file_path
				);
				diffuse_map = diffuse_map->next;
			}
		}
	}
	good &= fprintf(
		output_file,
		"	}\n"
	) >= 0;
	*current_material_name = '\0';
	face_count = 0;
	return true;
}
